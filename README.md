
**Stock Prices Prediction using LSTM**

The main objective of this project was to extract the AAPL Inc. stock prices data from yahoo finance and then predict the closing stock price of the cooperation (AAPL) by using past 60 days closing stock price data. In this project, I extracted the stock prices for the time period 7 years (January 2012 to December 2019) and then used LSTM to predict the closing stock price of the cooperation. The artificial neural network LSTM is widely used because it has ability to remember the past information which is important and forget the information which is not important.

**Data Preparation**

I created a new data frame containing only the target variable - Closing stock price and converted it into an array to train the model. Then for traning dataset, I took 80% of the data and scaled the dataset values between 0 and 1. It is usually a good practice to scale the date betweeb 0 and 1 so that all the data may lie in same range and model can converge faster. Then the training dataset was splitted into x_train and y_train data set which contained the past 60 days closing price value and used to predict the 61st closing price value. The LSTM network expects the input data to be provided in 3D array structure: [samples, time steps, features]. Currently, the data is in the form: [samples, features] I transformed the train and test input data into the expected structure using numpy.reshape(). The first dimension is the number of records or rows in the dataset. The second dimension is the number of time steps which is 60 while the last dimension is the number of indicators. I used only one feature, i.e Closing stock price so the number of indicators will be one.

**Building the model and making Predictions**

Artificial Neural Network "LSTM" is built using two LSTM layers with 50 neurons and two dense layers with 25 neurons and 1 neuron respectively to predict the closing stock price. The model was compiled using the mean squared error as loss function to reduce the loss and Adam optimizer. Lastly, the model was fit and passed to the training features and a test data set was created to get predictions.
